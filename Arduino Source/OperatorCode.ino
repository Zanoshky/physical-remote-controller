/*
 * Operator Side - Controller
 *
 * Autor: Marko Zanoski
 * eMail: marko@zanoski.com
 * Date: 2015-October
 *
 * Description:
 * This piece of code turns on relay that is connected on Digital pins form 1 - 8.
 */

// Declaring variable for incoming serial data
char relay = 0;
char operation = 0;

void setup()
{
    // Initializing serial communication at 9600 bits per second
    Serial.begin(9600);

    // Setting pins as output
    pinMode(1, OUTPUT);
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);

}

void loop()
{
    //if data is available then read from serial and save it into variable
    while (Serial.available() > 1){ 
    {
        relay = Serial.read();
        operation = Serial.read();
    }

    if (relay == '1')
    {
        if (operation == '1')
        {
            digitalWrite(1, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(1, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(1, HIGH);
            delay(500);
            digitalWrite(1, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(1, HIGH);
            delay(500);
            digitalWrite(1, LOW);
            delay(500);
            digitalWrite(1, HIGH);
            delay(500);
            digitalWrite(1, LOW);
        }
    }
    else if (relay == '2')
    {
        if (operation == '1')
        {
            digitalWrite(2, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(2, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(2, HIGH);
            delay(500);
            digitalWrite(2, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(2, HIGH);
            delay(500);
            digitalWrite(2, LOW);
            delay(500);
            digitalWrite(2, HIGH);
            delay(500);
            digitalWrite(2, LOW);
        }
    }
    else if (relay == '3')
    {
        if (operation == '1')
        {
            digitalWrite(3, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(3, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(3, HIGH);
            delay(500);
            digitalWrite(3, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(3, HIGH);
            delay(500);
            digitalWrite(3, LOW);
            delay(500);
            digitalWrite(3, HIGH);
            delay(500);
            digitalWrite(3, LOW);
        }
    }
    else if (relay == '4')
    {
        if (operation == '1')
        {
            digitalWrite(4, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(4, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(4, HIGH);
            delay(500);
            digitalWrite(4, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(4, HIGH);
            delay(500);
            digitalWrite(4, LOW);
            delay(500);
            digitalWrite(4, HIGH);
            delay(500);
            digitalWrite(4, LOW);
        }
    }
    else if (relay == '5')
    {
        if (operation == '1')
        {
            digitalWrite(5, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(5, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(5, HIGH);
            delay(500);
            digitalWrite(5, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(5, HIGH);
            delay(500);
            digitalWrite(5, LOW);
            delay(500);
            digitalWrite(5, HIGH);
            delay(500);
            digitalWrite(5, LOW);
        }
    }
    else if (relay == '6')
    {
        if (operation == '1')
        {
            digitalWrite(6, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(6, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(6, HIGH);
            delay(500);
            digitalWrite(6, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(6, HIGH);
            delay(500);
            digitalWrite(6, LOW);
            delay(500);
            digitalWrite(6, HIGH);
            delay(500);
            digitalWrite(6, LOW);
        }
    }
    else if (relay == '7')
    {
        if (operation == '1')
        {
            digitalWrite(7, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(7, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(7, HIGH);
            delay(500);
            digitalWrite(7, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(7, HIGH);
            delay(500);
            digitalWrite(7, LOW);
            delay(500);
            digitalWrite(7, HIGH);
            delay(500);
            digitalWrite(7, LOW);
        }
    }
    else if (relay == '8')
    {
        if (operation == '1')
        {
            digitalWrite(8, LOW);
        }
        else if (operation == '2')
        {
            digitalWrite(8, HIGH);
        }
        else if (operation == '3')
        {
            digitalWrite(8, HIGH);
            delay(500);
            digitalWrite(8, LOW);
            delay(500);
            operation = '0';
        }
        else if (operation == '4')
        {
            digitalWrite(8, HIGH);
            delay(500);
            digitalWrite(8, LOW);
            delay(500);
            digitalWrite(8, HIGH);
            delay(500);
            digitalWrite(8, LOW);
        }
    }
}