package prc.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import prc.model.Command;

public class SQLController {

    private final static String dbURL = "jdbc:mysql://localhost:3306";
    private final static String dbName = "prcmanager";
    private final static String username = "root";
    private final static String password = "";

    private static Connection getConnection() {
        Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(dbURL + "/" + dbName, username, password);
        } catch (SQLException e) {
            System.out.println("SQLController =>" + " getConnection => " + e.toString());
        } catch (ClassNotFoundException e) {
            System.out.println("SQLController =>" + " getConnection => " + e.toString());
        } catch (Exception e) {
            System.out.println("SQLController =>" + " getConnection => " + e.toString());
        }

        return conn;
    }

    public static Command getLatestCommand() {
        Command returnMe = null;

        try {
            Connection conn = SQLController.getConnection();
            Statement stmt = conn.createStatement();
            String sql = "SELECT * FROM command WHERE execute_on BETWEEN NOW() - INTERVAL 5 SECOND AND NOW() AND executed IS NULL ORDER BY command_id DESC LIMIT 1;";

            ResultSet rs = stmt.executeQuery(sql);
            rs.first();

            int id = rs.getInt("command_id");
            int deviceID = rs.getInt("device_id");
            Timestamp timestamp = rs.getTimestamp("timestamp");
            int value = rs.getInt("value");
            Timestamp executeOn = rs.getTimestamp("execute_on");

            returnMe = new Command(id, deviceID, value, timestamp, executeOn);
            conn.close();
        } catch (Exception e) {
            System.out.println("SQLController =>" + " getLatestCommand => No timmed command");

            try {
                Connection conn = SQLController.getConnection();
                Statement stmt = conn.createStatement();
                String sql = "SELECT * FROM command WHERE executed IS NULL AND execute_on IS NULL ORDER BY command_id DESC LIMIT 1;";

                ResultSet rs = stmt.executeQuery(sql);
                rs.first();

                int id = rs.getInt("command_id");
                int deviceID = rs.getInt("device_id");
                Timestamp timestamp = rs.getTimestamp("timestamp");
                int value = rs.getInt("value");
                Timestamp executeOn = rs.getTimestamp("execute_on");

                returnMe = new Command(id, deviceID, value, timestamp, executeOn);
                conn.close();
            } catch (Exception f) {
                System.out.println("SQLController =>" + " getLatestCommand => No instant command");
            }
        }

        return returnMe;
    }
    
    public static void setCommandExecuted(int id) {
        try {
            Connection conn = SQLController.getConnection();
            Statement stmt = conn.createStatement();
            String sql = "UPDATE command SET executed = 1 WHERE command_id=" + id + ";";
            stmt.executeUpdate(sql);
            conn.close();
        } catch (Exception e) {
            System.out.println("SQLController =>" + " setCommandExecuted => " + e.toString() + " => ID: " + id);
        }
    }
}
