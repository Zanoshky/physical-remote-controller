package prc.controller;

import prc.model.Command;
import jssc.SerialPort;
import jssc.SerialPortException;

public class UserlessServerController {

    private static char VALUE;
    private static char RELAY;
    private static Command NEW_COMMAND;

    public static void main(String[] args) throws InterruptedException {

        //Create serial connection with Arduino on COM3 port
        SerialPort serialPort = new SerialPort("COM3");
        System.out.println("UserlessServerController =>" + " main 1 => Serial Port COM3");

        //Opening and setting communication settings
        try {
            serialPort.openPort();
            serialPort.setParams(9600, 8, 1, 0);
        } catch (SerialPortException e) {
            System.out.println("UserlessServerController =>" + " main 2 => " + e.toString());
            return;
        }

        //Infinite loop to check for new commands
        while (true) {

            //Getting the last Command or null if none found.
            NEW_COMMAND = SQLController.getLatestCommand();

            //if we do have new command then
            if (NEW_COMMAND != null) {

                System.out.println("UserlessServerController =>" + " main 4 => NEW ID: " + NEW_COMMAND.getID());
                VALUE = (char) (NEW_COMMAND.getValue() + 48);
                RELAY = (char) (NEW_COMMAND.getDeviceID() + 48);

                System.out.println("UserlessServerController =>" + " main 5 => NEW COMAMND in WHILE loop => ID: " + NEW_COMMAND.getID() + " Relay: " + NEW_COMMAND.getDeviceID() + " Value: " + NEW_COMMAND.getValue());
                try {
                    serialPort.writeByte((byte) RELAY);
                    System.out.println("UserlessServerController =>" + " main 6 => writeByte => " + RELAY);
                    serialPort.writeByte((byte) VALUE);
                    System.out.println("UserlessServerController =>" + " main 7 => writeByte => " + VALUE);
                } catch (SerialPortException e) {
                    System.out.println("UserlessServerController =>" + " main 8 => writeByte => " + e.toString());
                    return;
                }

                SQLController.setCommandExecuted(NEW_COMMAND.getID());
            }

            //Pause for 1.5 seconds
            Thread.sleep(1500);
        }
    }
}
