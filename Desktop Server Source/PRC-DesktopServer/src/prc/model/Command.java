package prc.model;

import java.sql.Timestamp;

public class Command {

    private int id;
    private int deviceID;
    private int value;
    private Timestamp timestamp;
    private Timestamp executeOn;

    public Command(int id, int deviceID, int value, Timestamp timestamp, Timestamp executeOn) {
        this.id = id;
        this.value = value;
        this.timestamp = timestamp;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(int deviceID) {
        this.deviceID = deviceID;
    }

    public Timestamp getExecuteOn() {
        return executeOn;
    }

    public void setExecuteOn(Timestamp executeOn) {
        this.executeOn = executeOn;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
