# README #

This is source code of P.R.C. -  Physical Remote Controller.

It allows you to use combination of Arduino with Relays to remotely control any physical devices over Web Application.

# REQUIREMENTS #

* Arduino
* Relay
* Laptop
* Raspberry PI (optional)
* Ethernet Module (optional)

# SETUP #
Download Arudino IDE to edit Arduino Source files.

* Link: https://www.arduino.cc/en/Main/Software

Download NetBeans to edit Desktop Server Source files.

* Link: https://netbeans.org/downloads/

Download MySQL Workbench to edit MySQL Database Source files.

* Link: https://dev.mysql.com/downloads/workbench/

Download AppGini to edit Web Application Source files.

* Link: http://bigprof.com/appgini/download

Download XAMPP to Setup Local Web Environment for testing or Live environment.

* Link: https://www.apachefriends.org/download.html